/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int colorprompt = 1;                /* -p  option; if 1, prompt uses SchemeSel, otherwise SchemeNorm */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"JetBrains Mono:size=11",
	"Noto Color Emoji:size=13:antialias=true:autohint=true",
	"Symbols Nerd Font:size=13"
};
static const char *prompt     = NULL;      /* -p  option; prompt to the left of input field */
static const char col_bg[]    = "#191E1E";
static const char col_fg[]    = "#C5B7AD";
static const char col_fg2[]   = "#3F4B4B";


static const char *colors[SchemeLast][2] = {
	/*     							          fg   		    bg       */
	[SchemeNorm] 			      = { col_fg,    col_bg },
	[SchemeSel] 			      = { col_bg,    col_fg },
	[SchemeNormHighlight] 	= { col_fg2,   col_bg },
	[SchemeSelHighlight] 	  = { col_bg,    col_fg },
	[SchemeOut] 			      = { "#000000", "#00ffff" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 10;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 27;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
